@extends('layouts/contentLayoutMaster')

@section('title', __('report.botReprt'))
@section('page-style')

@endsection
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif
<div class="row justify-content-md-center">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{__('report.postback')}}</th>
                    @foreach ($period as $key => $value)
                        <th>{{$value->format('m-d')}}</th>       
                    @endforeach
                    <th>{{__('report.total')}}</th>
                </tr>
            </thead>
            <tbody>
                    @foreach ($request->postback_ids as $postback_id)
                    <tr>
                    @php
                        $replayButton = \App\Models\ReplayButton::where('id',$postback_id)->first();
                        $postbackTotalCount = 0;
                    @endphp
                    <td>{{$replayButton->report_name}}</td>
                    @foreach ($period as $key => $value)
                        @php
                            $postbackcount = \App\Models\UserMessage::where('postback_id',$replayButton->postback_id)->whereDay('created_at', '=', $value->format('d'))->whereMonth('created_at', '=', $value->format('m'))->whereYear('created_at', '=', $value->format('Y'))->count();
                            $postbackTotalCount += $postbackcount;
                        @endphp
                        <td>{{$postbackcount}}</td>
                    @endforeach
                    <td>{{$postbackTotalCount}}</td>
                </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('page-script')

@endsection
