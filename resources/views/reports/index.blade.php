@extends('layouts/contentLayoutMaster')

@section('title', __('report.title'))
@section('page-style')
<link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}" />
@endsection
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text" style="direction: ltr">

      <div id="line-area-chart"></div>

    </div>
  </div>
</div>

@endsection
@section('page-script')
<script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
@php
  $days = days_in_month(date('m'), date('Y'));
@endphp
<script>
  var areaChartEl = document.querySelector('#line-area-chart'),
    areaChartConfig = {
      chart: {
        height: 400,
        type: 'area',
        parentHeightOffset: 0,
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: false,
        curve: 'straight'
      },
      legend: {
        show: true,
        position: 'top',
        horizontalAlign: 'start'
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        }
      },
      colors: [
        '#a4f8cd',
        '#2bdac7'
      ],
      series: [
        {
          name: 'Messages',
          data: [
            @for ($i=1;$i<=$days;$i++)
              {{\App\Models\UserMessage::whereDay('created_at', '=', $i)->whereMonth('created_at', '=', date('m'))->whereYear('created_at', '=', date('Y'))->count()}},
            @endfor
          ]
        },
        {
          name: 'Conversations',
          data: [
            @for ($i=1;$i<=$days;$i++)
              {{\App\Models\UserMessage::distinct('user_id')->whereDay('created_at', '=', $i)->whereMonth('created_at', '=', date('m'))->whereYear('created_at', '=', date('Y'))->count()}},
            @endfor
          ]
        }
      ],
      xaxis: {
        categories: [
          @for ($i=1;$i<=$days;$i++)
            '{{date('d/m',strtotime(date('Y-m-'.$i)))}}',
          @endfor
        ]
      },
      fill: {
        opacity: .7,
        type: 'solid'
      },
      tooltip: {
        shared: false
      },
      yaxis: {
        opposite: false
      }
    };
  if (typeof areaChartEl !== undefined && areaChartEl !== null) {
    var areaChart = new ApexCharts(areaChartEl, areaChartConfig);
    areaChart.render();
  }
</script>
@endsection
