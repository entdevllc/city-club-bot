@extends('layouts/contentLayoutMaster')

@section('title', __('report.title'))
@section('page-style')
<link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}" />
@endsection
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif
<div class="row justify-content-md-center">
    <div class="col-md-4 col-12">
        <div class="card">
        <div class="card-body">
            <div class="card-text">
                <form method="POST" action="{{route('fbReport.postbackReprtReport')}}" class="form" autocomplete="off">
                    @csrf
                    <div class="row">
                        <x-forms.select required="true" name="bot_id" class="col-md-12" inputClass="bot_id"  title="{{__('report.bot')}}" :items="[]" />
                        <x-forms.select required="true" name="postback_ids[]" class="col-md-12" multiple inputClass="postback_id"  title="{{__('report.postback')}}" :items="[]" />
                        <x-forms.inputText required="true" type="text" class="col-md-12" inputClass="flatpickr-basic" name="from" required="yes" title="{{__('report.from')}}" />
                        <x-forms.inputText required="true" type="text" class="col-md-12" inputClass="flatpickr-basic" name="to" required="yes" title="{{__('report.to')}}" />
                        
                        <div class="col-12">
                            <x-forms.button class="primary" type="submit" title="{{__('report.show')}}" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
@section('page-script')
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script>
$( function() {
    if ($('.flatpickr-basic').length) {
        $('.flatpickr-basic').flatpickr();
    }
    $('.bot_id').select2({
        ajax: {
          url: "{{url(route('select.fbBotData'))}}",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });
    $('.postback_id').select2({
        ajax: {
          url: "{{url(route('select.fbReplay'))}}",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term,
              bot_id: $('.bot_id option:selected').val()
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

});
</script>
@endsection
