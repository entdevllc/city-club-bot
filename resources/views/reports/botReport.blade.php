@extends('layouts/contentLayoutMaster')

@section('title', __('report.botReprt'))
@section('page-style')
<link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}" />
@endsection
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text" style="direction: ltr">

      <div id="line-area-chart"></div>

    </div>
  </div>
</div>

@endsection
@section('page-script')
<script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>

<script>
  var areaChartEl = document.querySelector('#line-area-chart'),
    areaChartConfig = {
      chart: {
        height: 400,
        type: 'area',
        parentHeightOffset: 0,
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: false,
        curve: 'straight'
      },
      legend: {
        show: true,
        position: 'top',
        horizontalAlign: 'start'
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        }
      },
      colors: [
        '#a4f8cd',
        '#2bdac7'
      ],
      series: [
        {
          name: 'Messages',
          data: [
            @foreach ($period as $key => $value)
              {{\App\Models\UserMessage::where('page_id',$page_id)->whereDay('created_at', '=', $value->format('d'))->whereMonth('created_at', '=', $value->format('m'))->whereYear('created_at', '=', $value->format('Y'))->count()}},
            @endforeach
          ]
        },
        {
          name: 'Conversations',
          data: [
            @foreach ($period as $key => $value)
              {{\App\Models\UserMessage::where('page_id',$page_id)->distinct('user_id')->whereDay('created_at', '=', $value->format('d'))->whereMonth('created_at', '=', $value->format('m'))->whereYear('created_at', '=', $value->format('Y'))->count()}},
            @endforeach
          ]
        }
      ],
      xaxis: {
        categories: [
          @foreach ($period as $key => $value)
            '{{$value->format('m/d')}}',
          @endforeach
        ]
      },
      fill: {
        opacity: .7,
        type: 'solid'
      },
      tooltip: {
        shared: false
      },
      yaxis: {
        opposite: false
      }
    };
  if (typeof areaChartEl !== undefined && areaChartEl !== null) {
    var areaChart = new ApexCharts(areaChartEl, areaChartConfig);
    areaChart.render();
  }
</script>
@endsection
