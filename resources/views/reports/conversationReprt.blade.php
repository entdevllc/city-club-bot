@extends('layouts/contentLayoutMaster')

@section('title', __('report.botReprt'))
@section('page-style')

@endsection
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif
@php
    $conversation = \App\Models\Conversation::with('conversationQuestion')->where('id',$request->conversation_id)->first();
@endphp
<div class="row justify-content-md-center">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    @foreach ($conversation->conversationQuestion as $question)
                        <th>{{$question->title}}</th>       
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php
                    $conversationUser = \App\Models\ConversationUser::with('conversationAnswer')
                                        ->whereDate('created_at','>=',$request->from)
                                        ->whereDate('created_at','<=',$request->to)
                                        ->where('conversation_id',$request->conversation_id)
                                        ->get();
                    
                @endphp
                @foreach ($conversationUser as $item)
                    <tr>
                        @foreach ($conversation->conversationQuestion as $question)
                        <td>{{$item->conversationAnswer->where('conversation_question_id',$question->id)->first()->answer}}</td>
                        @endforeach
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>
@endsection
@section('page-script')

@endsection