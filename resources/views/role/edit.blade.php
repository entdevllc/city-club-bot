@extends('layouts/contentLayoutMaster')

@section('title', __('common.edit').' '.__('role.role'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">
        <form method="POST" action="{{route('role.update',['role'=>$role->id])}}" class="form" autocomplete="off">
            <input type="hidden" name="_method" value="PUT">
            @csrf
            <div class="row">
                <x-forms.inputText 
                    type="text"
                    name="name"
                    value="{{$role->name}}"
                    title="{{__('role.role')}}"
                        />
            
                <div class="col-12">
                    <x-forms.checkBox
                        name="permission[]"
                        title="{{__('role.permission')}}"
                        :selected="$role->permissions()->get()->pluck('id')->toArray()"
                        :items="$permission" />
                </div>
                <div class="col-12">
                    <x-forms.button 
                        class="primary"
                        type="submit"
                        title="{{__('common.save')}}"
                            />
                    
                    <x-forms.button 
                        class="secondary"
                        type="reset"
                        title="{{__('common.reset')}}"
                            />
                </div>
            </div>
        </form>
    </div>
  </div>
</div>

@endsection
