@extends('layouts/contentLayoutMaster')

@section('title', __('role.roles'))
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="roles"
          action="role.action"
          title="{{__('role.roles')}}"
          :colums="[['name',__('role.role')],['permission',__('role.permission')]]"
          link="{{route('dataTable.role')}}"
          addNew="{{route('role.create')}}"
          canAdd="{{\auth::user()->can('Super Admin')}}"
          canEdit="{{\auth::user()->can('Super Admin')}}"
          />

    </div>
  </div>
</div>

@endsection
