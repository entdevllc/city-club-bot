@extends('layouts/contentLayoutMaster')

@section('title', __('fbBot.addReplay'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">
        @if(isset($replay->id))
        <form method="POST" action="{{route('fbReplay.update')}}" class="form" autocomplete="off">
        @else
        <form method="POST" action="{{route('fbReplay.store')}}" class="form" autocomplete="off">
        @endif
            @csrf
            <input type="hidden" name="replay_button_id" value="{{isset($replayButton->id)?$replayButton->id:0}}" />
            <input type="hidden" name="type" value="text" />
            <input type="hidden" name="fb_bot_id" value="{{$fbBot->id}}" />
            @if(isset($replayButton->id))

            <a href="{{route('fbReplay.index')}}/{{$fbBot->id}}/{{$replayButton->replay->replay_button_id}}/" class="btn btn-outline-dark waves-effect">
                <i data-feather='arrow-right'></i>  {{__('fbBot.back')}}  
            </a>
            <br>
            <br>
            @endif
            <div class="row">
                    @if(isset($replayButton->id))
                    <x-forms.inputText class="col-md-4" inputClass="title" type="text" name="title" value="{{isset($replayButton->title)?$replayButton->title:''}}" title="{{__('fbBot.title')}}" />
                    <x-forms.inputText class="col-md-4" inputClass="report_name" type="text" name="report_name" value="{{isset($replayButton->report_name)?$replayButton->report_name:''}}" title="{{__('report.reportName')}}" />
                    @endif
                    <x-forms.textArea class="col-md-12" name="text" title="{{__('fbBot.text')}}" value="{{isset($replay->text)?$replay->text:''}}" />

                    @if(isset($replay->buttons))
                    <h2>{{__('fbBot.buttons')}}</h2>
                    <div class="col-12 mb-5">
                        @foreach ($replay->buttons as $button)
                            <a href="{{route('fbReplay.store')}}/{{$fbBot->id}}/{{$button->id}}" class="btn btn-icon btn-primary">{{$button->title}}</a>
                        @endforeach
                        
                    </div>
                    @endif
                    <h2>{{__('fbBot.addbuttons')}}</h2>
                    <div class="repeater-default col-12">
                        <div data-repeater-list="buttons">
                            <div data-repeater-item>
                                <div class="row d-flex align-items-end new-option">
                                <!--<x-forms.select class="col-md-4" inputClass="button_type" name="button_type" title="{{__('fbBot.button_type')}}" :items="$buttonType" />-->
        
                                <x-forms.inputText class="col-md-4" inputClass="title" type="text" name="title" title="{{__('fbBot.title')}}" />
        
                                <x-forms.select 
                                    name="buttonType"
                                    class="col-md-3"
                                    inputClass="buttonType"
                                    title="{{__('fbBot.button_type')}}"
                                    :items="[
                                        ['value'=>'text','title'=>'الرد بالنص'],
                                        ['value'=>'backToHome','title'=>'عرض القائمة الرئيسية'],
                                        ['value'=>'stopChat','title'=>'ايقاف البوت'],
                                        ['value'=>'conversation','title'=>'CRM'],
                                    ]" />

                                <x-forms.select 
                                    name="conversation"
                                    style="display: none"
                                    class="col-md-3 conversationDiv"
                                    inputClass="conversation"
                                    title="{{__('fbBot.conversation')}}"
                                    :items="$conversation" />
                                
                                <x-forms.inputText
                                    class="col-md-4 valuDive"
                                    style="display: none"
                                    type="text"
                                    name="value"
                                    title="{{__('fbBot.value')}}" />
                                
                                
                                <div class="col-md-1 col-12 mb-50">
                                    <div class="form-group">
                                    <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                        <i data-feather="x" class="mr-25"></i>
                                        <span>{{__('common.delete')}}</span>
                                    </button>
                                    </div>
                                </div>
                                </div>
                                <hr>
                            </div>
                        </div>

                        <div class="col-12">
                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                              <i data-feather="plus" class="mr-25"></i>
                              <span>{{__('common.add')}}</span>
                            </button>
                        </div>
                        <br>
                        <br>
                    </div>
                <div class="col-12">


                    <x-forms.button 
                        class="primary"
                        type="submit"
                        title="{{__('common.save')}}"
                            />
                    
                    <x-forms.button 
                        class="secondary"
                        type="reset"
                        title="{{__('common.reset')}}"
                            />


                    @if(isset($replayButton->id))
                        <a href="{{route('fbReplay.destroy',['fbReplay'=>$replayButton->id])}}" class="btn btn-danger waves-effect waves-float waves-light">{{__('fbBot.deleteReplay')}}</a>
                    @endif
                </div>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection


@section('vendor-script')
<script src="{{asset('vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
@endsection
@section('page-script')
<script>
$( function() {
    $('.buttonType').select2("destroy");

    $('.buttonType').change(function(){
        if($("option:selected", this).val()=="conversation")
        {
            $(this).closest('.new-option').find('.conversationDiv').show()
        }else{

            $(this).closest('.new-option').find('.conversationDiv').hide()
        }

        if($("option:selected", this).val()=="stopChat")
        {
            $(this).closest('.new-option').find('.valuDive').show()
        }else{

            $(this).closest('.new-option').find('.valuDive').hide()
        }

    });

    $('.buttons-repeater, .repeater-default').repeater({
        show: function () {
        
        $(this).slideDown();
        // Feather Icons
        if (feather) {
            feather.replace({ width: 14, height: 14 });
        }
        },
        hide: function (deleteElement) {
        if (confirm('{{__('sale.deleteSkill')}}')) {
            $(this).slideUp(deleteElement);
        }
        }
    });
});
</script>
@endsection