@extends('layouts/contentLayoutMaster')

@section('title', __('fbBot.fbBots'))
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="fbBots"
          action="fbBot.action"
          title="{{__('fbBot.fbBots')}}"
          :colums="[['title',__('fbBot.title')],['created_at',__('common.created_at')]]"
          link="{{route('dataTable.fbBot')}}"
          addNew="{{route('fbBot.create')}}"
          canAdd="{{\auth::user()->can('Add fbBot')}}"
          canEdit="{{\auth::user()->can('Edit fbBot')}}"
          />

    </div>
  </div>
</div>

@endsection
