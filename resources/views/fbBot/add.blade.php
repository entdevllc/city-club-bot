@extends('layouts/contentLayoutMaster')

@section('title', __('common.add'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">
        <form method="POST" action="{{route('fbBot.store')}}" class="form" autocomplete="off">
            @csrf
            <div class="row">
                <x-forms.inputText 
                    type="text"
                    name="title"
                    required="yes"
                    title="{{__('fbBot.title')}}"
                        />
                <x-forms.select name="page_id" inputClass="page_id"  title="{{__('fbBot.page')}}" :items="[]" />
                
                <div class="col-12">
                    <x-forms.button 
                        class="primary"
                        type="submit"
                        title="{{__('common.save')}}"
                            />
                    
                    <x-forms.button 
                        class="secondary"
                        type="reset"
                        title="{{__('common.reset')}}"
                            />
                </div>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
$( function() {
    $('.page_id').select2({
        ajax: {
          url: "{{url(route('select.fbPageData'))}}",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

});
</script>
@endsection
