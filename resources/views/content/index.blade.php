@extends('layouts/contentLayoutMaster')

@section('title', getSetting('name'))
@section('content')

<!-- Page layout -->
<div class="card">
  <div class="card-header">
  </div>
  <div class="card-body">
    <div class="card-text text-center">
      <img src="{{url('/images/logo/logo.png')}}" class="img-fluid" />
    </div>
  </div>
</div>
<!--/ Page layout -->

@endsection
