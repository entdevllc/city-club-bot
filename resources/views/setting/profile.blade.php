@extends('layouts/contentLayoutMaster')

@section('title', __('profile.editData'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">
        <form method="POST" action="{{route('profile.store')}}" class="form" enctype="multipart/form-data" autocomplete="off">
            @csrf
            <div class="row">

                        
                <x-forms.inputText type="password" name="password" title="{{__('profile.password')}}" />
                <x-forms.inputText type="password" name="repassword" title="{{__('profile.repassword')}}" />
                <x-forms.file name="avatar" title="{{__('profile.avatar')}}" />
                   

                <div class="col-12">
                    <x-forms.button class="primary" type="submit" title="{{__('common.save')}}" />
                    
                    <x-forms.button class="secondary" type="reset" title="{{__('common.reset')}}" />
                </div>
            </div>
        </form>
    </div>
  </div>
</div>




          
@endsection
