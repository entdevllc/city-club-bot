@extends('layouts/contentLayoutMaster')

@section('title', __('setting.setting'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">


        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" aria-controls="home" role="tab" aria-selected="true">{{__('setting.general')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="uploading-tab" data-toggle="tab" href="#uploading" aria-controls="uploading" role="tab" aria-selected="false">{{__('setting.uploading')}}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="replaySetting-tab" data-toggle="tab" href="#replaySetting" aria-controls="replaySetting" role="tab" aria-selected="false">{{__('setting.replaySetting')}}</a>
            </li>
          </ul>

        <form method="POST" action="{{route('setting.store')}}" class="form tab-content" autocomplete="off">
            @csrf
            <div class="tab-pane active" id="general" aria-labelledby="general-tab" role="tabpanel">
              <div class="row">
                    <x-forms.inputText 
                        type="text"
                        name="name"
                        value="{{getSetting('name')}}"
                        title="{{__('user.name')}}"
                            />

                    <x-forms.inputText 
                        type="text"
                        name="facebook_app_secret"
                        value="{{getSetting('facebook_app_secret')}}"
                        title="{{__('setting.facebook_app_secret')}}"
                            />

                    <x-forms.inputText 
                        type="text"
                        name="facebook_verification"
                        value="{{getSetting('facebook_verification')}}"
                        title="{{__('setting.facebook_verification')}}"
                            />
              </div>
            </div>
            <div class="tab-pane" id="uploading" aria-labelledby="uploading-tab" role="tabpanel">
              <div class="row">
                    <x-forms.inputText 
                      type="text"
                      name="uploadFileExtImage"
                      value="{{getSetting('uploadFileExtImage')}}"
                      title="{{__('setting.uploadFileExtImage')}}"
                          />
                    <x-forms.inputText 
                      type="text"
                      name="uploadFileExtDoc"
                      value="{{getSetting('uploadFileExtDoc')}}"
                      title="{{__('setting.uploadFileExtDoc')}}"
                          />
              </div>
            </div>
            <div class="tab-pane" id="replaySetting" aria-labelledby="replaySetting-tab" role="tabpanel">
              <div class="row">
                    
                    <x-forms.select 
                      name="noMatchReplay"
                      title="{{__('setting.noMatchReplay')}}"
                      :selected="[getSetting('noMatchReplay')]"
                      :items="[
                        ['value'=>'text','title'=>'الرد بالنص'],
                        ['value'=>'returnToHome','title'=>'عرض القائمة الرئيسية'],
                        ['value'=>'NoReplay','title'=>'لا يتم الرد'],
                      ]" />
                    <x-forms.inputText 
                      type="text"
                      name="noMatchReplayText"
                      value="{{getSetting('noMatchReplayText')}}"
                      title="{{__('setting.noMatchReplayText')}}"
                          />
              </div>
            </div>
            <div class="col-12">
              <x-forms.button 
                  class="primary"
                  type="submit"
                  title="{{__('common.save')}}"
                      />
            </div>
        </form>
    </div>
  </div>
</div>

@endsection
