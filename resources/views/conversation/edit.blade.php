@extends('layouts/contentLayoutMaster')

@section('title', __('common.edit'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">
        <form method="POST" action="{{route('conversation.update',['conversation'=>$conversation->id])}}" class="form" autocomplete="off">
            <input type="hidden" name="_method" value="PUT">
            @csrf
            <div class="row">
                <x-forms.inputText 
                    type="text"
                    name="title"
                    value="{{$conversation->title}}"
                    title="{{__('conversation.title')}}"
                        />
                        <x-forms.select  name="fb_bot_id" inputClass="fb_bot_id" title="{{__('conversation.bot_id')}}" :items="[['title'=>$conversation->fbBot?->title,'value'=>$conversation->fb_bot_id]]" />
                
                
                <x-forms.inputText 
                  type="text"
                  name="complete_text"
                  required="yes"
                  value="{{$conversation->complete_text}}"
                  title="{{__('conversation.complete_text')}}"
                      />
                <div class="col-12">
                    <x-forms.button 
                        class="primary"
                        type="submit"
                        title="{{__('common.save')}}"
                            />
                    
                    <x-forms.button 
                        class="secondary"
                        type="reset"
                        title="{{__('common.reset')}}"
                            />
                </div>
            </div>
        </form>
    </div>
  </div>
</div>



<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="conversationQuestion"
          action="conversation.actionQuestion"
          title="{{__('conversation.conversationQuestion')}}"
          :colums="[['title',__('conversation.title')],['type',__('conversation.questionType')],['text',__('conversation.question')],['val',__('conversation.val')],['created_at',__('common.created_at')]]"
          link="{{route('dataTable.conversationQuestion')}}?conversation_id={{$conversation->id}}"
          canEdit="{{\auth::user()->can('Edit conversation')}}"
          />

    </div>
  </div>
</div>

<div class="card">
    <div class="card-body">
      <div class="card-header border-bottom">
        <h4 class="card-title">{{__('conversation.addQuestion')}}</h4>
      </div>
      <div class="card-text">
          <form method="POST" action="{{route('conversationQuestion.store')}}" class="form" autocomplete="off">
              @csrf
              <input type="hidden" name="conversation_id" value="{{$conversation->id}}"/>
              <div class="row">
                  <x-forms.inputText 
                      type="text"
                      name="title"
                      required="yes"
                      title="{{__('conversation.title')}}"
                          />
                          
                  <x-forms.inputText 
                    type="text"
                    name="text"
                    required="yes"
                    title="{{__('conversation.question')}}"
                        />

                  <x-forms.select
                    name="type"
                    title="{{__('conversation.questionType')}}"
                    :items="[
                      ['value'=>'free_text','title'=>'Free Text'],
                      ['value'=>'user_email','title'=>'User Email'],
                      ['value'=>'user_phone_number','title'=>'User Phone'],
                      ['value'=>'full_name','title'=>'User Full Name'],
                      ['value'=>'defined_data','title'=>'Defined Data'],
                    ]" />
                  
                  <x-forms.inputText 
                    type="text"
                    name="val"
                    title="{{__('conversation.val')}}"
                        />
                  
                  <div class="col-12">
                      <x-forms.button 
                          class="primary"
                          type="submit"
                          title="{{__('common.save')}}"
                              />
                      
                      <x-forms.button 
                          class="secondary"
                          type="reset"
                          title="{{__('common.reset')}}"
                              />
                  </div>
              </div>
          </form>
      </div>
    </div>
</div>
@endsection

@section('page-script')
<script>
$( function() {
    $('.fb_bot_id').select2({
        ajax: {
          url: "{{url(route('select.fbBotData'))}}",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

});
</script>
@endsection
