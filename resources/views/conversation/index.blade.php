@extends('layouts/contentLayoutMaster')

@section('title', __('conversation.conversations'))
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="conversations"
          action="conversation.action"
          title="{{__('conversation.conversations')}}"
          :colums="[['title',__('conversation.title')],['created_at',__('common.created_at')]]"
          link="{{route('dataTable.conversation')}}"
          addNew="{{route('conversation.create')}}"
          canAdd="{{\auth::user()->can('Add conversation')}}"
          canEdit="{{\auth::user()->can('Edit conversation')}}"
          />

    </div>
  </div>
</div>

@endsection
