<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}">
    
    <div class="form-group">
        <label for="customFile">{{$title}}</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="{{$name}}" id="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}" @if($attributes['required']) required @endif>
            <label class="custom-file-label" for="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}">{{__('common.chooseFile')}}</label>
        </div>
    </div>
</div>