<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}">
    <div class="form-group">
        <label for="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}">{{$title}}</label>
        <textarea rows="{{isset($attributes['rows'])?$attributes['rows']:'3'}}" class="form-control" name="{{$name}}" id="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}" placeholder="{{isset($attributes['placeholder'])?$attributes['placeholder']:''}}" autocomplete="off" @if($attributes['required']) required @endif>{{isset($attributes['value'])?$attributes['value']:''}}</textarea>
    </div>
</div>