<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}" style="{{isset($attributes['style'])?$attributes['style']:''}}">
    <div class="form-group">
        <label for="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}">{{$title}}</label>
        <input type="{{$type}}" class="form-control {{isset($attributes['inputClass'])?$attributes['inputClass']:''}}" name="{{$name}}" id="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}" placeholder="{{isset($attributes['placeholder'])?$attributes['placeholder']:''}}" value="{{isset($attributes['value'])?$attributes['value']:''}}" autocomplete="off" @if($attributes['required']) required @endif @if($attributes['disabled']) disabled @endif>
    </div>
</div>