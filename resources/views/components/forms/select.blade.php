<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}" style="{{isset($attributes['style'])?$attributes['style']:''}}">
    <div class="form-group">
        <label for="{{$name}}">{{$title}}</label>
        <select name="{{$name}}" id="{{isset($attributes['id'])?$attributes['id']:$name}}" @if($attributes['required']) required @endif @if($attributes['multiple']) multiple @endif class="select2 form-control form-control-lg  {{isset($attributes['inputClass'])?$attributes['inputClass']:''}}">
            @if($attributes['allOption']=='yes')
                <option value="0" @if(in_array(0,$selected)) selected @endif>{{__('common.unspecified')}}</option>
            @endif
                @foreach ($multipleSelect as $item)
                    <option value="{{$item['value']}}" selected>{{$item['title']}}</option>
                @endforeach
            @foreach ($items as $item)
                <option value="{{$item['value']}}" @if(in_array($item['value'],$selected)) selected @endif>{{$item['title']}}</option>
            @endforeach
        </select>
    </div>
</div>