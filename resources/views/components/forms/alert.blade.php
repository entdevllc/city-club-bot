<div class="alert alert-{{$type}}" role="alert">
    <h4 class="alert-heading">{{$title}}</h4>
    <div class="alert-body">
        {!!$text!!}
    </div>
</div>