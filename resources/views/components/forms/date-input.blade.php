<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}">
    <div class="form-group">
        <label for="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}">{{$title}}</label>
        <input type="text" class="form-control flatpickr-basic flatpickr-input" name="{{$name}}" id="{{isset($attributes['id'])?$attributes['id']:$name.'input'}}" placeholder="{{isset($attributes['placeholder'])?$attributes['placeholder']:''}}" value="{{isset($attributes['value'])?$attributes['value']:''}}" autocomplete="off" @if($attributes['required']) required @endif placeholder="YYYY-MM-DD">
    </div>
</div>