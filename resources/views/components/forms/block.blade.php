<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}">
    <div class="form-group">
        <label>{{$title}}</label>
        <div class="{{isset($attributes['inputClass'])?$attributes['inputClass']:''}}">
            {{$value}}
        </div>
    </div>
</div>