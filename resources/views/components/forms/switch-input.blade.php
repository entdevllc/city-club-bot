<div class="col-sm-6 col-12 {{isset($attributes['class'])?$attributes['class']:''}}">
    <div class="form-group">
        <label for="{{isset($attributes['id'])?$attributes['id']:''}}">{{$title}}</label>
        @foreach ($items as $item)
            <div class="custom-control custom-control-primary custom-switch">
                <input type="checkbox" id="{{$name.$item['value']}}" name="{{$name}}" class="custom-control-input" value="{{$item['value']}}" @if(in_array($item['value'],$selected)) checked @endif>
                <label class="custom-control-label" for="{{$name.$item['value']}}">{{$item['title']}}</label>
            </div>
        @endforeach
    </div>
</div>