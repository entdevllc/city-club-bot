<section id="{{$name}}-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header border-bottom">
            <h4 class="card-title">{{$title}}</h4>
          </div>
          <div class="card-datatable">
            <table class="{{$name}} table">
              <thead>
                <tr>
                  <th>#</th>
                  @foreach ($colums as $col)
                      <th>{{$col[1]}}</th>
                  @endforeach
                  @if($attributes['action'])
                      <th>{{__('common.action')}}</th>
                  @endif
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>


@section('page-script')
<script>
  $.fn.dataTable.ext.errMode = 'none';

var dt_filter_table = $('.{{$name}}');
if (dt_filter_table.length) {
  var dt_filter = dt_filter_table.DataTable({

    processing: true,
    serverSide: true,
    "order": [[0, 'desc']],
    ajax: '{!!$link!!}',
    columns: [
      { data: 'id' },
    @php
      $c = 0;
    @endphp
    @foreach ($colums as $col)
      @if ($c==0)
      { data: function(row, type, val, meta) {

              @if($attributes['canEdit'])
                return '<a href="'+location.protocol + '//' + location.host + location.pathname+'/'+row.id+'/edit">' + row.{{$col[0]}} + '</a>';
              @else
                return row.{{$col[0]}};
              @endif
      }},
      @php
        $c++;
      @endphp
      @else
      { data: '{{$col[0]}}' },
      @endif
    @endforeach

    @if($attributes['action'])
      { data: function(row, type, val, meta){
        var actionTpl = `@include($attributes['action'])`;
        return actionTpl;
      }}
    @endif
    ],
    dom:
        '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    orderCellsTop: true,
    dom:
        '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
        '<"col-lg-12 col-xl-6" l>' +
        '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
        '>t' +
        '<"d-flex justify-content-between mx-2 row mb-1"' +
        '<"col-sm-12 col-md-6"i>' +
        '<"col-sm-12 col-md-6"p>' +
        '>',

    
    buttons: [
    @if($attributes['addNew']&&$attributes['canAdd'])
        {
          text: '{{__('common.add')}}',
          className: 'add-new btn btn-primary mt-50',
          action: function ( e, dt, button, config ) {
            window.location = '{{$attributes['addNew']}}';
          },
          init: function (api, node, config) {
            $(node).removeClass('btn-secondary');
          }
        }
    @endif
    ],
      

    language: {
        url: '//cdn.datatables.net/plug-ins/1.10.24/i18n/Arabic.json'
    }
  });
}

// Advanced Search
// --------------------------------------------------------------------

</script>
@endsection
