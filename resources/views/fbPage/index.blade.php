@extends('layouts/contentLayoutMaster')

@section('title', __('fbPage.fbPages'))
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="fbPages"
          action="fbPage.action"
          title="{{__('fbPage.fbPages')}}"
          :colums="[['title',__('fbPage.title')],['created_at',__('common.created_at')]]"
          link="{{route('dataTable.fbPage')}}"
          addNew="{{route('fbPage.create')}}"
          canAdd="{{\auth::user()->can('Add fbPage')}}"
          canEdit="{{\auth::user()->can('Edit fbPage')}}"
          />

    </div>
  </div>
</div>

@endsection
