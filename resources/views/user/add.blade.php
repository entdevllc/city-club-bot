@extends('layouts/contentLayoutMaster')

@section('title', __('common.add'))
@section('content')
@if($errors->any())
<x-forms.alert title="{{__('common.error')}}" text="{!!'- '.implode('<br />- ',(array) $errors->all())!!}" type="danger" />
@endif

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">
        <form method="POST" action="{{route('user.store')}}" class="form" autocomplete="off">
            @csrf
            <div class="row">
                <x-forms.inputText 
                    type="text"
                    name="name"
                    required="yes"
                    title="{{__('user.name')}}"
                        />
            
                <x-forms.inputText 
                    type="text"
                    name="username"
                    required="yes"
                    title="{{__('user.username')}}"
                        />
                
                <x-forms.inputText 
                    type="password"
                    name="password"
                    title="{{__('auth.Password')}}"
                        />
                
                <x-forms.select 
                    name="role_id"
                    title="{{__('user.role')}}"
                    :items="$roles" />
                    
                <div class="col-12">
                    <x-forms.button 
                        class="primary"
                        type="submit"
                        title="{{__('common.save')}}"
                            />
                    
                    <x-forms.button 
                        class="secondary"
                        type="reset"
                        title="{{__('common.reset')}}"
                            />
                </div>
            </div>
        </form>
    </div>
  </div>
</div>

@endsection
