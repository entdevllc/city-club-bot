<div class="dropdown">
    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow waves-effect waves-float waves-light" data-toggle="dropdown" aria-expanded="false">
      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
    </button>
    <div class="dropdown-menu" style="">
      @can('Edit User')
        <a class="dropdown-item text-center" href="{{route('user.index')}}/`+row.id+`/edit">
          <span>{{__('common.edit')}}</span>
        </a>
      @endcan
      @can('Delete User')
        <form action="{{route('user.index')}}/`+row.id+`" method="POST">
          <input type="hidden" name="_method" value="DELETE">
          @csrf
          <button class="btn btn-flat-danger waves-effect btn-block">{{__('common.delete')}}</button>
        </form>
      @endcan
    </div>
</div>