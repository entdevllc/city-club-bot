@extends('layouts/contentLayoutMaster')

@section('title', __('user.users'))
@section('content')

@if(\Session::has('message'))
<x-forms.alert title="{{__('common.success')}}" text="{{\Session::get('message')}}" type="success" />
@endif

<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="users"
          action="user.action"
          title="{{__('user.users')}}"
          :colums="[['name',__('user.name')],['username',__('user.username')],['created_at',__('common.created_at')]]"
          link="{{route('dataTable.user')}}"
          addNew="{{route('user.create')}}"
          canAdd="{{\auth::user()->can('Add User')}}"
          canEdit="{{\auth::user()->can('Edit User')}}"
          />

    </div>
  </div>
</div>

@endsection
