@extends('layouts/contentLayoutMaster')

@section('title', __('common.log'))
@section('content')

<div class="card">
  <div class="card-body">
    <div class="card-text">

        <x-forms.dataTable
          name="logs"
          title="{{__('common.log')}}"
          :colums="[['text',__('common.action')],['username',__('user.username')],['created_at',__('common.created_at')]]"
          link="{{route('dataTable.log')}}"
          />

    </div>
  </div>
</div>

@endsection
