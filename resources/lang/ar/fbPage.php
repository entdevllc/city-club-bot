<?php
return [
    'fbPages' => 'الصفحات',
    'title' => 'الاسم',
    'page_id' => 'Page ID',
    'token' => 'التوكن',
    'successAdd' => 'تم إضافة الصفحة بنجاح',
    'successUpdate'=> 'تم تعديل الصفحة بنجاح',
    'successDelete'=> 'تم حذف الصفحة بنجاح',
];