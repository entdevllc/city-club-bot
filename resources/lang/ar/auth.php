<?php
return [
    'failed'   => 'لا تتطابق بيانات الاعتماد هذه مع سجلاتنا.',
    'throttle' => 'محاولات تسجيل الدخول كثيرة جدًا. يرجى المحاولة مرة أخرى خلال :seconds ثواني.',
    'Welcome to Sign!' => 'مرحباً بك',
    'Please sign-in to your account' => 'نرجو تسجيل الدخول',
    'Username' => 'اسم المستخدم',
    'Password' => 'كلمة المرور',
    'Remember Me' => 'تذكر كلمة المرور',
    'Sign in' => 'تسجيل الدخول'
];