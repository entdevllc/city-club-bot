<?php
return [
    'roles' => 'الصلاحيات',
    'name' => 'الاسم',
    'role' => 'الصلاحية',
    'permission' => 'الأذون المسموحة',
    'successAdd' => 'تم إضافة الصلاحية بنجاح',
    'successUpdate'=> 'تم تعديل الصلاحية بنجاح',
    'successDelete'=> 'تم حذف الصلاحية بنجاح',
];