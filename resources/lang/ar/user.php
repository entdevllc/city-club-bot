<?php
return [
    'users' => 'المستخدمين',
    'name' => 'الاسم',
    'username' => 'اسم المستخدم',
    'role' => 'الصلاحية',
    'userType' => 'نوع العضوية',
    'notSpecified' => 'غير محدد',
    'clientContact' => 'جهة اتصال عميل',
    'purcheseRep' => 'مندوب مشتريات',
    'worker' => 'عامل',
    'successAdd' => 'تم إضافة المستخدم بنجاح',
    'successUpdate'=> 'تم تعديل المستخدم بنجاح',
    'successDelete'=> 'تم حذف المستخدم بنجاح',
];