<?php
return [
    'conversations' => 'نماذج البيانات',
    'title' => 'الاسم',
    'bot_id' => 'البوت',
    'addQuestion' => 'إضافة سؤال',
    'question' => 'السؤال',
    'questionType' => 'النوع',
    'val' => 'القيم الإضافية',
    'complete_text' => 'رد استلام مدخلات النموذج',
    'conversationQuestion' => 'اسئلة النموذج',
    'successAdd' => 'تم إضافة لنموذج بنجاح',
    'successUpdate'=> 'تم تعديل لنموذج بنجاح',
    'successDelete'=> 'تم حذف لنموذج بنجاح',
];