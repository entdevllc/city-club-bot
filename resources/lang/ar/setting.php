<?php
return [
    'setting' => 'الإعدادات',
    'general' => 'الإعدادات العامة',
    'successUpdate' => 'تم تحديث الإعدادات بنجاح',
    'uploading' => 'إعدادات الرفع',
    'replaySetting' => 'إعدادات الردود',
    'noMatchReplay' => 'الرد في حالة وجود اختيار متطابق',
    'noMatchReplayText' => 'نص عدم الرد ( يستخدم مع الرد النصي فقط )',
    'facebook_token' => 'Token',
    'facebook_app_secret' => 'App Secret',
    'facebook_verification' => 'verification',
    'uploadFileExtImage' => 'ملفات الصور المسموح بها',
    'uploadFileExtDoc' => 'ملفات المستندات المسموح بها',
];