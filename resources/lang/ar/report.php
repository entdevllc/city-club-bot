<?php
return [
    'title' => 'التقارير',
    'postbackReprt' => 'تقارير عن رسائل',
    'botReprt' => 'تقرير عن صفحة',
    'reportName' => 'المسمى في التقارير',
    'conversationReprt' => 'تقرير عن النماذج',
    'form' => 'النموذج',
    'show' => 'عرض',
    'from' => 'من',
    'to' => 'الى',
    'postback' => 'الرد',
    'total' => 'الإجمالي',
    'bot' => 'البوت'
];