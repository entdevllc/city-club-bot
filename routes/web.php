<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ConversationController;
use App\Http\Controllers\ConversationQuestionController;
use App\Http\Controllers\FacebookBotController;
use App\Http\Controllers\FbBotController;
use App\Http\Controllers\FbPageController;
use App\Http\Controllers\FbReplayController;
use App\Http\Controllers\FbReportController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\WebChatController;

Auth::routes();
Route::get('logout', [LoginController::class, 'logout'])->name('logout');


Route::get('/chat', function(){
    return view('chat');
})->name('chat');

Route::get('/webChat',[WebChatController::class, 'index'])->name('webChat');
Route::post('/webChat',[WebChatController::class, 'index'])->name('webChat');
Route::match(['get', 'post'],'/chatBot', [FacebookBotController::class,'index'])->name('chatBot');

Route::middleware(['auth:web'])->group(function () {

    Route::get('/', [HomeController::class, 'home'])->name('home');
    Route::get('/home', [HomeController::class, 'home'])->name('home');
    // Other Routes
    Route::get('/log', [LogController::class, 'index'])->name('log.index');
    Route::get('/setting', [SettingController::class, 'index'])->name('setting.index');
    Route::post('/setting', [SettingController::class, 'store'])->name('setting.store');
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::post('/profile', [ProfileController::class, 'store'])->name('profile.store');
    
    Route::get('/fbReplay/{fbBot}/{replayButton}', [FbReplayController::class, 'create'])->name('fbReplay.create');
    Route::get('/fbReplay', [FbReplayController::class, 'index'])->name('fbReplay.index');
    Route::post('/fbReplay', [FbReplayController::class, 'store'])->name('fbReplay.store');
    Route::get('/deleteFbReplay/{fbReplay}', [FbReplayController::class, 'destroy'])->name('fbReplay.destroy');
    Route::post('/updateFbReplay', [FbReplayController::class, 'update'])->name('fbReplay.update');
    Route::get('/fbReport', [FbReportController::class, 'index'])->name('fbReport.index');
    Route::get('/postbackReprtForm', [FbReportController::class, 'postbackReprtForm'])->name('fbReport.postbackReprtForm');
    Route::post('/postbackReprtReport', [FbReportController::class, 'postbackReprtReport'])->name('fbReport.postbackReprtReport');
    Route::get('/botReprtForm', [FbReportController::class, 'botReprtForm'])->name('fbReport.botReprtForm');
    Route::post('/botReprt', [FbReportController::class, 'botReprt'])->name('fbReport.botReprt');
    Route::get('/conversationReprt', [FbReportController::class, 'conversationReprtForm'])->name('fbReport.conversationReprtForm');
    Route::post('/conversationReprt', [FbReportController::class, 'conversationReprt'])->name('fbReport.conversationReprt');
    
    // Resources
    Route::resources([
        'user' => UserController::class,
        'role' => RoleController::class,
        'fbPage' => FbPageController::class,
        'fbBot' => FbBotController::class,
        'conversation' => ConversationController::class,
        'conversationQuestion' => ConversationQuestionController::class
    ]);
    // Data Table
    Route::get('/dataTable/role', [RoleController::class, 'dataTable'])->name('dataTable.role');
    Route::get('/dataTable/user', [UserController::class, 'dataTable'])->name('dataTable.user');
    Route::get('/dataTable/log', [LogController::class, 'dataTable'])->name('dataTable.log');
    Route::get('/dataTable/fbPage', [FbPageController::class, 'dataTable'])->name('dataTable.fbPage');
    Route::get('/dataTable/fbBot', [FbBotController::class, 'dataTable'])->name('dataTable.fbBot');
    Route::get('/dataTable/conversation', [ConversationController::class, 'dataTable'])->name('dataTable.conversation');
    Route::get('/dataTable/conversationQuestion', [ConversationQuestionController::class, 'dataTable'])->name('dataTable.conversationQuestion');
    
    // Select 2
    Route::get('/select/user', [UserController::class, 'userData'])->name('select.userData');
    Route::get('/select/fbPage', [FbPageController::class, 'fbPageData'])->name('select.fbPageData');
    Route::get('/select/fbBot', [FbBotController::class, 'fbBotData'])->name('select.fbBotData');
    Route::get('/select/conversation', [ConversationController::class, 'conversationData'])->name('select.conversationData');
    Route::get('/select/fbReplay', [FbReplayController::class, 'fbReplayData'])->name('select.fbReplay');

    

});
// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);
