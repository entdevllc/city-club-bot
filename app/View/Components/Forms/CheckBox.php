<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class CheckBox extends Component
{
    public $title;
    public $name;
    public $items;
    public $selected;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $name, $items, $selected = [])
    {
        $this->title = $title;
        $this->items = $items;
        $this->name = $name;
        $this->selected = $selected;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.check-box');
    }
}
