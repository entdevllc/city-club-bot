<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Button extends Component
{
    public $title;
    public $class;
    public $type;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $class, $type)
    {
        $this->title = $title;
        $this->class = $class;
        $this->type = $type;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.button');
    }
}
