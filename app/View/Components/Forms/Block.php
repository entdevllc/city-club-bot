<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Block extends Component
{
    public $title;
    public $value;
    
    public function __construct($title, $value)
    {
        $this->title = $title;
        $this->value = $value;
    }

    public function render()
    {
        return view('components.forms.block');
    }
}
