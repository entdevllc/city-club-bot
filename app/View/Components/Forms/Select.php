<?php

namespace App\View\Components\Forms;

use App\Helpers\Assets;
use Illuminate\View\Component;

class Select extends Component
{
    public $title;
    public $name;
    public $items;
    public $selected;
    public $multipleSelect;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $name, $items, $selected=[],$multipleSelect=[])
    {
        $this->title = $title;
        $this->items = $items;
        $this->name = $name;
        $this->selected = $selected;
        $this->multipleSelect = $multipleSelect;
        Assets::setScriptAssets('select2','/vendors/js/forms/select/select2.full.min.js');
        Assets::setScriptAssets('select2-form','/js/scripts/forms/form-select2.js');
        Assets::setStyleAssets('select2','/vendors/css/forms/select/select2.min.css');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.select');
    }
}
