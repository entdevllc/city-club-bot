<?php

namespace App\View\Components\Forms;

use App\Helpers\Assets;
use Illuminate\View\Component;

class DateInput extends Component
{
    public $title;
    public $name;
    public function __construct($title, $name)
    {
        $this->title = $title;
        $this->name = $name;
        Assets::setScriptAssets('flatpickr','/vendors/js/pickers/flatpickr/flatpickr.min.js');
        Assets::setScriptAssets('form-pickers','/js/scripts/forms/pickers/form-pickers.js');
        Assets::setStyleAssets('flatpickr','/vendors/css/pickers/flatpickr/flatpickr.min.css');
        Assets::setStyleAssets('form-flat-pickr','/css/base/plugins/forms/pickers/form-flat-pickr.css');
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.date-input');
    }
}
