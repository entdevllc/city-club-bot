<?php

namespace App\View\Components\Forms;

use App\Helpers\Assets;
use Illuminate\View\Component;

class DataTable extends Component
{
    public $title;
    public $name;
    public $colums;
    public $link;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $name, $colums, $link)
    {
        $this->title = $title;
        $this->name = $name;
        $this->colums = $colums;
        $this->link = $link;
        //laod scripts
        Assets::setScriptAssets('jquery.dataTables','/vendors/js/tables/datatable/jquery.dataTables.min.js');
        Assets::setScriptAssets('datatables.bootstrap4','/vendors/js/tables/datatable/datatables.bootstrap4.min.js');
        Assets::setScriptAssets('dataTables.responsive','/vendors/js/tables/datatable/dataTables.responsive.min.js');
        Assets::setScriptAssets('responsive.bootstrap4','/vendors/js/tables/datatable/responsive.bootstrap4.js');
        Assets::setScriptAssets('datatables.buttons','/vendors/js/tables/datatable/datatables.buttons.min.js');
        Assets::setScriptAssets('buttons.bootstrap4','/vendors/js/tables/datatable/buttons.bootstrap4.min.js');
        //load styles
        Assets::setStyleAssets('dataTables.bootstrap4','/vendors/css/tables/datatable/dataTables.bootstrap4.min.css');
        Assets::setStyleAssets('responsive.bootstrap4','/vendors/css/tables/datatable/responsive.bootstrap4.min.css');
        Assets::setStyleAssets('buttons.bootstrap4','/vendors/css/tables/datatable/buttons.bootstrap4.min.css');

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.data-table');
    }
}
