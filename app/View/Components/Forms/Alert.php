<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Alert extends Component
{
    public $title;
    public $text;
    public $type;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $text, $type)
    {
        $this->title = $title;
        $this->text = $text;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.alert');
    }
}
