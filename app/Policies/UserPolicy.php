<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('View User');
    }

    public function view(User $user, User $model)
    {
        return $user->can('View User');
    }

    public function create(User $user)
    {
        return $user->can('Add User');
    }

    public function update(User $user, User $model)
    {
        return $user->can('Edit User');
    }

    public function delete(User $user, User $model)
    {
        return $user->can('Delete User');
    }

    public function restore(User $user, User $model)
    {
        //
    }

    public function forceDelete(User $user, User $model)
    {
        //
    }
}
