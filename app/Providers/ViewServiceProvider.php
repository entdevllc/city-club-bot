<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Auth\Guard; 
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Guard $auth)
    {
        View::composer('*', function ($view) use ($auth) {
            $view->with('currentUser', $auth->user());
        });
    }
}