<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Models\FbPage;
use App\Services\FbPageService;
use Illuminate\Http\Request;

class FbPageController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(FbPageService $service)
    {
        $this->authorizeResource(fbPage::class);
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('fbPage.fbPages')]
        ];
        return view('fbPage.index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create(Request $request)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('fbPage.index'),'name'=>__('fbPage.fbPages')], ['name'=>__('common.add')]
        ];
        return view('fbPage.add', ['breadcrumbs' => $breadcrumbs]);
    }
    
    public function store(Request $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('fbPage.successAdd'));
        }
    }

    public function edit(FbPage $fbPage)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('fbPage.index'),'name'=>__('fbPage.fbPages')], ['name'=>__('common.edit')]
        ];
        return view('fbPage.edit', ['breadcrumbs' => $breadcrumbs,'fbPage' => $fbPage]);
    }

    public function update(Request $request, FbPage $fbPage)
    {
        if($this->service->update($request,$fbPage))
        {
            return back()->with('message', __('fbPage.successUpdate'));
        }
    }
    
    public function destroy(FbPage $fbPage)
    {
        if($this->service->delete($fbPage))
        {
            return back()->with('message', __('fbPage.successDelete'));
        }
    }
    
    public function fbPageData(Request $request)
    {
        $fbPages =$this->service->model->where('title', 'LIKE' , '%'.$request->q.'%');
        return $fbPages->get()->transform(function ($item, $key) {
                return ['text'=>$item->title,'id'=>$item->id];
            });
    }
}
