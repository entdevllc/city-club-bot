<?php

namespace App\Http\Controllers;

use App\Services\SettingService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public $service;

    public function __construct(SettingService $service)
    { 
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('profile.editData')]
        ];
        return view('setting.profile', ['breadcrumbs' => $breadcrumbs]);
    }

    public function store(Request $request)
    {
        if($this->service->updateProfile($request))
        {
            return back()->with('message', __('setting.successUpdate'));
        }
    }
}
