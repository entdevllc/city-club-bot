<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Http\Requests\Role\RoleStoreRequest;
use App\Http\Requests\Role\RoleUpdateRequest;
use App\Services\RoleService;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(RoleService $service)
    {
        $this->middleware(['role:Super Admin']); 
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('role.roles')]
        ];
        return view('role.index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('role.index'),'name'=>__('role.roles')], ['name'=>__('common.add')]
        ];
        $permission = Permission::all()->transform(function ($item, $key) {
            return ['title'=>$item->name,'value'=>$item->id];
        });
        return view('role.add', ['breadcrumbs' => $breadcrumbs, 'permission' => $permission]);
    }
    
    public function store(RoleStoreRequest $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('role.successAdd'));
        }
    }

    public function edit(Role $role)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('role.index'),'name'=>__('role.roles')], ['name'=>__('common.edit')]
        ];
        $permission = Permission::all()->transform(function ($item, $key) {
            return ['title'=>$item->name,'value'=>$item->id];
        });
        return view('role.edit', ['breadcrumbs' => $breadcrumbs,'role' => $role, 'permission' => $permission]);
    }

    public function update(RoleUpdateRequest $request, Role $role)
    {
        if($this->service->update($request,$role))
        {
            return back()->with('message', __('role.successUpdate'));
        }
    }
    
    public function destroy(Role $role)
    {
        if($this->service->delete($role))
        {
            return back()->with('message', __('role.successDelete'));
        }
    }

    public function dataTableColumns($value)
    {
        $value['permission'] = implode(',',Auth::user()->getAllPermissions()->pluck('name')->toArray());
        return $value;
    }
}
