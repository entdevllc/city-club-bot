<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Services\LogService;
use Illuminate\Http\Request;

class LogController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(LogService $service)
    {
        $this->middleware(['role:Super Admin']); 
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('common.log')]
        ];
        return view('log.index', ['breadcrumbs' => $breadcrumbs]);
    }


    public function dataTableColumns($value)
    {
        $value['username'] = $value->user->name;
        return $value;
    }

}
