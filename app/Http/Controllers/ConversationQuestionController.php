<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Models\ConversationQuestion;
use App\Services\ConversationQuestionService;
use Illuminate\Http\Request;

class ConversationQuestionController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(ConversationQuestionService $service)
    {
        $this->authorizeResource(ConversationQuestionController::class);
        $this->service = $service;
    }
    
    public function store(Request $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('conversation.successAdd'));
        }
    }

    
    public function destroy(ConversationQuestion $conversationQuestion)
    {
        if($this->service->delete($conversationQuestion))
        {
            return back()->with('message', __('conversation.successDelete'));
        }
    }
    public function dataTableSearch(Request $request)
    {
        $search = [];

        if(isset($request->conversation_id))
        {
            $search[] = ['field'=>'conversation_id','exp'=>'=','value'=>$request->conversation_id];
        }
        return $search;
    }
}