<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(UserService $service)
    {
        $this->authorizeResource(User::class);
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('user.users')]
        ];
        return view('user.index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create(Request $request)
    {
        //$this->authorize('create',User::class);
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('user.index'),'name'=>__('user.users')], ['name'=>__('common.add')]
        ];
        $roles = Role::all()->transform(function ($item, $key) {
            return ['title'=>$item->name,'value'=>$item->id];
        });
        $userTypes = collect($this->service->userTypes())->transform(function ($item, $key) {
            return ['title'=>$item,'value'=>$key];
        });
        return view('user.add', ['breadcrumbs' => $breadcrumbs, 'roles' => $roles,'userTypes' => $userTypes]);
    }
    
    public function store(UserStoreRequest $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('user.successAdd'));
        }
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('user.index'),'name'=>__('user.users')], ['name'=>__('common.edit')]
        ];
        $roles = Role::all()->transform(function ($item, $key) {
            return ['title'=>$item->name,'value'=>$item->id];
        });
        $userTypes = collect($this->service->userTypes())->transform(function ($item, $key) {
            return ['title'=>$item,'value'=>$key];
        });
        $selectedTypes = count($user->type->pluck('type_id'))>0?$user->type->pluck('type_id')->toArray():[];
        return view('user.edit', ['breadcrumbs' => $breadcrumbs,'user' => $user, 'roles' => $roles,'userTypes' => $userTypes, 'selectedTypes' => $selectedTypes]);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        if($this->service->update($request,$user))
        {
            return back()->with('message', __('user.successUpdate'));
        }
    }
    
    public function destroy(User $user)
    {
        if($this->service->delete($user))
        {
            return back()->with('message', __('user.successDelete'));
        }
    }
    
    public function userData(Request $request)
    {
        $users =$this->service->model->where('name', 'LIKE' , '%'.$request->q.'%');
        if(isset($request->user_type))
        {
            $users->where('user_type',$request->user_type);
        }
        return $users->get()->transform(function ($item, $key) {
                return ['text'=>$item->name,'id'=>$item->id];
            });
    }
}