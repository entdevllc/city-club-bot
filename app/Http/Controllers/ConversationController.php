<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Models\Conversation;
use App\Services\ConversationService;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(ConversationService $service)
    {
        $this->authorizeResource(Conversation::class);
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('conversation.conversations')]
        ];
        return view('conversation.index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create(Request $request)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('conversation.index'),'name'=>__('conversation.conversations')], ['name'=>__('common.add')]
        ];
        return view('conversation.add', ['breadcrumbs' => $breadcrumbs]);
    }
    
    public function store(Request $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('conversation.successAdd'));
        }
    }

    public function edit(Conversation $conversation)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('conversation.index'),'name'=>__('conversation.conversations')], ['name'=>__('common.edit')]
        ];
        return view('conversation.edit', ['breadcrumbs' => $breadcrumbs,'conversation' => $conversation]);
    }

    public function update(Request $request, Conversation $conversation)
    {
        if($this->service->update($request,$conversation))
        {
            return back()->with('message', __('conversation.successUpdate'));
        }
    }
    
    public function destroy(Conversation $conversation)
    {
        if($this->service->delete($conversation))
        {
            return back()->with('message', __('conversation.successDelete'));
        }
    }
    
    public function conversationData(Request $request)
    {
        $conversations =$this->service->model->where('title', 'LIKE' , '%'.$request->q.'%');
        return $conversations->get()->transform(function ($item, $key) {
                return ['text'=>$item->title,'id'=>$item->id];
            });
    }
}
