<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\FbBot;
use App\Models\FbReplay;
use App\Models\ReplayButton;
use App\Services\FbReplayService;
use Illuminate\Http\Request;

class FbReplayController extends Controller
{

    public $service;

    public function __construct(FbReplayService $service)
    {
        $this->service = $service;
    }

    public function index(){}

    public function create(FbBot $fbBot,$replayButton)
    {

        $replay = FbReplay::where('replay_button_id',$replayButton)->first();
        
        $replayButton = $replayButton!=0?ReplayButton::find($replayButton):0;

        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('fbBot.index'),'name'=>__('fbBot.fbBots')], ['name'=>__('fbBot.addReplay')]
        ];
        $buttonType = [
            [
                'title'=>'Postback',
                'value' => 'postback'
            ],
            [
                'title'=>'Image',
                'value' => 'image'
            ],
            [
                'title'=>'Video',
                'value' => 'video'
            ],
            [
                'title'=>'Audio',
                'value' => 'audio'
            ],
            [
                'title'=>'Location',
                'value' => 'location'
            ],
        ];

        $conversation = Conversation::where('fb_bot_id',$fbBot->id)->get()->transform(function ($item, $key) {
            return ['title'=>$item->title,'value'=>$item->id];
        });;
        return view('fbBot.replay', ['breadcrumbs' => $breadcrumbs, 'buttonType' => $buttonType, 'fbBot' => $fbBot, 'replayButton' => $replayButton, 'replay' => $replay, 'conversation' => $conversation]);
    }

    public function store(Request $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('fbBot.successAdd'));
        }
    }

    public function update(Request $request)
    {
        if($this->service->update($request))
        {
            return back()->with('message', __('fbBot.successAdd'));
        }
    }
    
    public function destroy(ReplayButton $fbReplay)
    {
        if($route = $this->service->delete($fbReplay))
        {
            return redirect()->route('fbReplay.create',$route)->with('message', __('fbBot.successDelete'));
        }
    }

    
    public function fbReplayData(Request $request)
    {
        $replay = FbReplay::where('fb_bot_id',$request->bot_id)->pluck('id');
        $fbPages = ReplayButton::whereIN('fb_replay_id',$replay)->where('report_name', 'LIKE' , '%'.$request->q.'%')->whereNotNull('report_name');
        return $fbPages->get()->transform(function ($item, $key) {
                return ['text'=>$item->report_name,'id'=>$item->id];
            });
    }
}
