<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\ConversationAnswer;
use App\Models\ConversationUser;
use App\Models\UserCurrentAction;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Facebook\Extensions\QuickReplyButton;

class FacebookBotConversationController extends Controller
{
    public function conversation($userID, $conversation_id, $bot)
    {
        $conversation = Conversation::find($conversation_id);
        $userCurrentAction = UserCurrentAction::where('user_id',$userID)
                            ->where('current_action','conversation')
                            ->whereDate('stop_until', '>', date('Y-m-d H:i:s'));
        // If conversation not stated yet
        if($userCurrentAction->count()==0)
        {
            // get question
            $conversationQuestion = $conversation->conversationQuestion()->orderBy('id','asc')->first();
            $this->createConversationStop($userID, $conversation->id, $conversationQuestion->id);
        }else{
            $userCurrentActionRow = $userCurrentAction->first();
            // save user reply
            $this->saveUserReplay($userID, $conversation_id, $userCurrentActionRow->step_id, $userCurrentActionRow->conversation_user_id, $bot->getMessage()->getText());
            // get new question
            $conversationQuestion = $conversation->conversationQuestion()
                                    ->where('id','>',$userCurrentActionRow->step_id)
                                    ->orderBy('id','asc')->first();
        }
        // send question
        if(isset($conversationQuestion->text))
        {
            $this->changeCurrentActtion($userID, $conversation->id, $conversationQuestion->id);
            if($conversationQuestion->type == 'full_name')
            {
                $user = $bot->getUser();
                $buttons = [
                    QuickReplyButton::create($user->getFirstName().' '.$user->getLastName())->type('text')->payload($user->getFirstName().' '.$user->getLastName())
                ];
            }
            if($conversationQuestion->type == 'user_email')
            {
                $buttons = [
                    QuickReplyButton::create('email')->type('user_email')
                ];
            }
            if($conversationQuestion->type == 'user_phone_number')
            {
                $buttons = [
                    QuickReplyButton::create('Phone')->type('user_phone_number')
                ];
            }
            if($conversationQuestion->type == 'defined_data')
            {
                $data = explode(',',$conversationQuestion->val);
                $buttons = [];
                foreach($data as $item)
                {
                    $buttons[] = QuickReplyButton::create($item)->type('text')->payload($item);
                }
            }
            $bot->reply(Question::create($conversationQuestion->text)->addButtons($buttons));
        }else{
            // send complete text
            $this->removeStop($userID);
            $bot->reply($conversation->complete_text);
        }
    }
    public function changeCurrentActtion($userID, $conversation_id, $step_id)
    {

        return UserCurrentAction::where('user_id',$userID)->update(['action_id'=>$conversation_id,'step_id'=>$step_id]);
    }
    public function removeStop($userID)
    {
        return UserCurrentAction::where('user_id',$userID)->delete();
    }

    public function createConversationStop($userID,$conversation_id, $step_id)
    {
        $conversation_user = new ConversationUser();
        $conversation_user->user_id = $userID;
        $conversation_user->conversation_id = $conversation_id;
        $conversation_user->save();
        
        $userCurrentAction = new UserCurrentAction();
        $userCurrentAction->user_id = $userID;
        $userCurrentAction->current_action = 'conversation';
        $userCurrentAction->action_id = $conversation_id;
        $userCurrentAction->step_id = $step_id;
        $userCurrentAction->conversation_user_id = $conversation_user->id;
        $userCurrentAction->stop_until = date('Y-m-d H:i:s',strtotime('+1440 minutes'));
        $userCurrentAction->save();
    }

    public function saveUserReplay($userID, $conversation_id, $step_id, $conversation_user_id, $replay)
    {
        $userReplay = new ConversationAnswer();
        $userReplay->user_id = $userID;
        $userReplay->conversation_question_id = $step_id;
        $userReplay->conversation_id = $conversation_id;
        $userReplay->answer = $replay;
        $userReplay->conversation_user_id = $conversation_user_id;
        $userReplay->save();
    }

}