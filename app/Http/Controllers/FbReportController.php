<?php

namespace App\Http\Controllers;

use App\Models\FbBot;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class FbReportController extends Controller
{
  public function index(){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('common.fbReport')]
    ];
    return view('/reports/index', ['breadcrumbs' => $breadcrumbs]);
  }

  public function botReprtForm(){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('report.botReprt')]
    ];
    return view('/reports/botReportForm', ['breadcrumbs' => $breadcrumbs]);
  }

  public function botReprt(Request $request){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('report.botReprt')]
    ];
    $period = new DatePeriod(
      new DateTime($request->from),
      new DateInterval('P1D'),
      new DateTime($request->to)
    );
    $page_id = FbBot::where('id',$request->bot_id)->first()->fbPage->page_id;
    return view('/reports/botReport', ['breadcrumbs' => $breadcrumbs, 'period' => $period, 'request' => $request, 'page_id' => $page_id]);
  }

  public function postbackReprtForm(){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('report.postbackReprt')]
    ];
    return view('/reports/postbackReprtForm', ['breadcrumbs' => $breadcrumbs]);
  }

  public function postbackReprtReport(Request $request){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('report.postbackReprt')]
    ];
    $period = new DatePeriod(
      new DateTime($request->from),
      new DateInterval('P1D'),
      new DateTime($request->to)
    );
    return view('/reports/postbackReprt', ['breadcrumbs' => $breadcrumbs, 'request' => $request, 'period' => $period]);
  }

  public function conversationReprtForm(){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('report.conversationReprt')]
    ];
    return view('/reports/conversationReprtForm', ['breadcrumbs' => $breadcrumbs]);
  }

  public function conversationReprt(Request $request){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('report.conversationReprt')]
    ];
    return view('/reports/conversationReprt', ['breadcrumbs' => $breadcrumbs, 'request' => $request]);
  }
  
}