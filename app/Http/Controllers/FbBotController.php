<?php

namespace App\Http\Controllers;

use App\Helpers\DataTable;
use App\Models\FbBot;
use App\Services\FbBotService;
use Illuminate\Http\Request;

class FbBotController extends Controller
{
    use DataTable;
    public $service;

    public function __construct(FbBotService $service)
    {
        $this->authorizeResource(fbBot::class);
        $this->service = $service;
    }

    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('fbBot.fbBots')]
        ];
        return view('fbBot.index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function create(Request $request)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('fbBot.index'),'name'=>__('fbBot.fbBots')], ['name'=>__('common.add')]
        ];
        return view('fbBot.add', ['breadcrumbs' => $breadcrumbs]);
    }
    
    public function store(Request $request)
    {
        if($this->service->create($request))
        {
            return back()->with('message', __('fbBot.successAdd'));
        }
    }

    public function edit(FbBot $fbBot)
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['link'=>route('fbBot.index'),'name'=>__('fbBot.fbBots')], ['name'=>__('common.edit')]
        ];
        return view('fbBot.edit', ['breadcrumbs' => $breadcrumbs,'fbBot' => $fbBot]);
    }

    public function update(Request $request, FbBot $fbBot)
    {
        if($this->service->update($request,$fbBot))
        {
            return back()->with('message', __('fbBot.successUpdate'));
        }
    }
    
    public function destroy(FbBot $fbBot)
    {
        if($this->service->delete($fbBot))
        {
            return back()->with('message', __('fbBot.successDelete'));
        }
    }
    
    public function fbBotData(Request $request)
    {
        $fbBots =$this->service->model->where('title', 'LIKE' , '%'.$request->q.'%');
        return $fbBots->get()->transform(function ($item, $key) {
                return ['text'=>$item->title,'id'=>$item->id];
            });
    }
}
