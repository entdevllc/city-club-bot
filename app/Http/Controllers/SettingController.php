<?php

namespace App\Http\Controllers;

use App\Services\SettingService;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public $service;

    public function __construct(SettingService $service)
    {
        $this->middleware(['role:Super Admin']); 
        $this->service = $service;
    }


    public function index()
    {
        $breadcrumbs = [
            ['link'=>"home",'name'=>__('common.home')], ['name'=>__('setting.setting')]
        ];
        return view('setting.index', ['breadcrumbs' => $breadcrumbs]);
    }

    public function store(Request $request)
    {
        if($this->service->updateSetting($request))
        {
            return back()->with('message', __('setting.successUpdate'));
        }
    }
}
