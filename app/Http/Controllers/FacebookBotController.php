<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\FbPage;
use App\Models\FbReplay;
use App\Models\ReplayButton;
use App\Models\UserCurrentAction;
use App\Models\UserMessage;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\QuickReplyButton;
use BotMan\Drivers\Facebook\FacebookDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FacebookBotController extends Controller
{
    public function index(Request $request)
    {
        $config = [];
        if(isset($request->entry[0]['id']))
        {
            $page = FbPage::where('page_id',$request->entry[0]['id'])->first();
            $config['facebook']['token'] = $page->token;
        }
        

        $config['facebook']['app_secret'] = getSetting('facebook_app_secret');
        $config['facebook']['verification'] = getSetting('facebook_verification');
        
        DriverManager::loadDriver(FacebookDriver::class);
        
        $botman = BotManFactory::create($config);
        
        $botman->hears('.*', function (BotMan $bot) {
            
            $payload = $bot->getMessage()->getPayload();
            if(isset($payload['postback']['payload']) && $payload['postback']['payload']!='')
            {
                $this->userMessage($payload['recipient']['id'],$payload['sender']['id'],$payload['postback']['payload']);
                
                $first_msg = ReplayButton::where('postback_id',$bot->getMessage()->getText())->first();

                if($first_msg->type == 'text')
                {
                    $this->textReplay($first_msg, $bot);
                }
                if($first_msg->type == 'stopChat')
                {
                    $this->stopChat($payload['sender']['id'],$first_msg->val);
                }
                if($first_msg->type == 'backToHome')
                {
                    $this->returnToHome($bot);
                }
                if($first_msg->type == 'conversation')
                {
                    $conversation = new FacebookBotConversationController();
                    $conversation->conversation($payload['sender']['id'], $first_msg->val, $bot);
                }
            }else{
                if(!isset($payload['recipient']['id'])&&!isset($payload['sender']['id']))
                {
                    return;
                }
                $this->userMessage($payload['recipient']['id'],$payload['sender']['id'],'text');
                $userAction =$this->userInConversation($payload['sender']['id']); 
                if($userAction->count()>0)
                {
                    $conversation = new FacebookBotConversationController();
                    $conversation->conversation($payload['sender']['id'], $userAction->first()->action_id, $bot);
                }elseif($this->chatIsStoped($payload['sender']['id'])==0)
                {
                    $this->noMatchReplay($bot);
                }
            }
        });
        $botman->listen();
    }
    private function noMatchReplay($bot)
    {

        $noMatchReplay = getSetting('noMatchReplay');
        if($noMatchReplay == 'returnToHome')
        {
            $this->returnToHome($bot);
        }elseif($noMatchReplay == 'text')
        {
            $bot->reply(getSetting('noMatchReplayText'));
        }
    }
    private function returnToHome($bot)
    {

        $first_msg = FbReplay::where('replay_button_id',0)->first();
        $buttons = $first_msg->buttons;
        $q = ButtonTemplate::create($first_msg->text);
        $c = 0;
        foreach($buttons as $botton)
        {
            if($c==3)
            {
                $bot->reply($q);
                $c=0;
                $q = ButtonTemplate::create('المزيد');
            }

            $q->addButton(ElementButton::create($botton->title)
                ->type('postback')
                ->payload($botton->postback_id)
            );

            $c++;
        }
        $bot->reply($q);
    }
    private function textReplay($first_msg, $bot)
    {
        $buttons = isset($first_msg->childReplay->buttons)?$first_msg->childReplay->buttons:[];
        if(count($buttons)>0)
        {
            $q = ButtonTemplate::create($first_msg->childReplay->text);
            $c = 0;
            foreach($buttons as $botton)
            {
                if($c==3)
                {
                    $bot->reply($q);
                    $c=0;
                    $q = ButtonTemplate::create('المزيد');
                }
                $q->addButton(ElementButton::create($botton->title)
                    ->type('postback')
                    ->payload($botton->postback_id)
                );
                $c++;
            }
            $bot->reply($q);
        }elseif(isset($first_msg->childReplay->text))
        {
            $bot->reply($first_msg->childReplay->text);
        }
    }
    private function userMessage($page_id,$user_id,$postback_id)
    {
        $userMessage = new UserMessage();
        $userMessage->page_id = $page_id;
        $userMessage->user_id = $user_id;
        $userMessage->postback_id = $postback_id;
        $userMessage->save();
    }
    private function stopChat($userID,$minutes)
    {
        $userCurrentAction = new UserCurrentAction();
        $userCurrentAction->user_id = $userID;
        $userCurrentAction->current_action = 'stopChat';
        $userCurrentAction->stop_until = date('Y-m-d H:i:s',strtotime('+'.$minutes.' minutes'));
        $userCurrentAction->save();
    }

    private function chatIsStoped($userID)
    {
        return UserCurrentAction::where('user_id',$userID)->whereDate('stop_until', '>', date('Y-m-d H:i:s'))->count();
    }

    private function userInConversation($userID)
    {
        return UserCurrentAction::where('user_id',$userID)->where('current_action','conversation')->whereDate('stop_until', '>', date('Y-m-d H:i:s'));
    }

}
