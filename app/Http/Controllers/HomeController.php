<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
  public function home(){
    $breadcrumbs = [
        ['link'=>"home",'name'=>getSetting('name')], ['name'=>__('common.home')]
    ];
    //auth()->user()->notify(new PurchaseOrder(Purchase::find(1)));
    return view('/content/index', ['breadcrumbs' => $breadcrumbs]);
  }
}