<?php

namespace App\Http\Controllers;

use App\Models\FbReplay;
use App\Models\ReplayButton;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Facebook\Extensions\ElementButton;

class WebChatController extends Controller
{
    public function index()
    {
        $config = [
            // Your driver-specific configuration
            // "telegram" => [
            //    "token" => "TOKEN"
            // ]
            
  	//'token' => 'YOUR-FACEBOOK-PAGE-TOKEN-HERE',
    //'app_secret' => 'YOUR-FACEBOOK-APP-SECRET-HERE',
    //'verification'=>'MY_SECRET_VERIFICATION_TOKEN',
        ];
        
        // Load the driver(s) you want to use
        DriverManager::loadDriver(\BotMan\Drivers\Web\WebDriver::class);
        
        // Create an instance
        $botman = BotManFactory::create($config);
        
        // Give the bot something to listen for.
        $botman->hears('.*', function (BotMan $bot) {

            if($bot->getMessage()->getPayload()['interactive']==1)
            {
                $first_msg = ReplayButton::where('postback_id',$bot->getMessage()->getText())->first();
                $buttons = [];
                if(isset($first_msg->childReplay))
                {
                    foreach($first_msg->childReplay->buttons as $botton)
                    {
                        $buttons[] = Button::create($botton->title)->value($botton->postback_id);
                    }
                }
                if(count($buttons)>0)
                {
                    $q = Question::create($first_msg->childReplay->text)->addButtons($buttons);
                }elseif(isset($first_msg->childReplay->text))
                {
                    $q = $first_msg->childReplay->text;
                }

                if(isset($q))
                {
                    $bot->reply($q);
                }
            }else{
                $first_msg = FbReplay::where('replay_button_id',0)->first();
                $buttons = [];
                foreach($first_msg->buttons as $botton)
                {
                    $buttons[] = Button::create($botton->title)->value($botton->postback_id);
                }
                $q = Question::create($first_msg->text)->addButtons($buttons);
                $bot->reply($q);
            }
        });
        
        // Start listening
        $botman->listen();
    }

}
