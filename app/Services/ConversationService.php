<?php

namespace App\Services;

use App\Models\Conversation;
use Illuminate\Http\Request;

class ConversationService extends Service
{
    public function model()
    {
        return 'App\Models\Conversation';
    }


    public function create(Request $request)
    {

        $conversation = $request->except(['_token','_method']);

        $savedPage = $this->model->create($conversation);

        $savedPage->log()->create(['text'=>'Create Conversation #'.$savedPage->id.' '.$savedPage->title,'user_id'=>auth()->user()->id]);
        
        return true;
        
    }

    public function update(Request $request,Conversation $conversation)
    {
        $conversationData = $request->except(['_token','_method']);
        
        $conversation->update($conversationData);
        $conversation->log()->create(['text'=>'update Conversation #'.$conversation->id.' '.$conversation->title,'user_id'=>auth()->user()->id]);
        return true;
    }

    public function delete(Conversation $conversation)
    {
        $conversation->log()->create(['text'=>'Delete Conversation #'.$conversation->id.' '.$conversation->title,'user_id'=>auth()->user()->id]);
        return $conversation->delete();
    }
}