<?php

namespace App\Services;

use App\Http\Requests\Role\RoleStoreRequest;
use App\Http\Requests\Role\RoleUpdateRequest;
use Spatie\Permission\Models\Role;

class RoleService extends Service
{
    
    public function model()
    {
        return 'Spatie\Permission\Models\Role';
    }

    public function create(RoleStoreRequest $request)
    {
        $role = $request->validated();
        $savedRole = $this->model->create(['name'=>$role['name']]);
        $savedRole->givePermissionTo($role['permission']);

        return true;
    }

    public function update(RoleUpdateRequest $request,Role $role)
    {
        $roleData = $request->validated();
        $role->update(['name'=>$roleData['name']]);
        $role->syncPermissions($roleData['permission']);
        return true;
    }

    public function delete(Role $role)
    {
        $role->syncPermissions([]);
        return $role->delete();
    }

}