<?php

namespace App\Services;

use App\Helpers\Uploads;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingService extends Service
{
    use Uploads;
    
    public function model()
    {
        return 'App\Models\Setting';
    }

    public function updateSetting(Request $request)
    {
        foreach($request->except(['_token']) as $var=>$val)
        {
            $this->model->updateOrCreate(['var'=>$var],['val'=>$val]);
        }
        $this->model->log()->create(['text'=>'Update Settings','user_id'=>auth()->user()->id]);
        return true;
    }

    public function updateProfile(Request $request)
    {
        $user = [];
        if(isset($request->password))
        {
            $request->validate([
                'password' => ['required'],
                'repassword' => ['same:password'],
            ]);
            $user['password'] = Hash::make($request->password);
        }
        if($request->hasFile('avatar'))
        {

            $user['avatar'] = $this->uploadImage($request->file('avatar'), 'image', 'users');
        }

        return User::where('id',auth()->user()->id)->update($user);

    }

}