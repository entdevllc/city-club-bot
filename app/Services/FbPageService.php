<?php

namespace App\Services;

use App\Models\FbPage;
use Illuminate\Http\Request;

class FbPageService extends Service
{
    public function model()
    {
        return 'App\Models\FbPage';
    }


    public function create(Request $request)
    {

        $page = $request->except(['_token','_method']);

        $savedPage = $this->model->create($page);

        $savedPage->log()->create(['text'=>'Create FbPage #'.$savedPage->id.' '.$savedPage->title,'user_id'=>auth()->user()->id]);
        
        return true;
        
    }

    public function update(Request $request,FbPage $page)
    {
        $pageData = $request->except(['_token','_method']);
        
        $page->update($pageData);
        $page->log()->create(['text'=>'update FbPage #'.$page->id.' '.$page->title,'user_id'=>auth()->user()->id]);
        return true;
    }

    public function delete(FbPage $page)
    {
        $page->log()->create(['text'=>'Delete FbPage #'.$page->id.' '.$page->title,'user_id'=>auth()->user()->id]);
        return $page->delete();
    }
}