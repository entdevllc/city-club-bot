<?php

namespace App\Services;

use App\Models\ConversationQuestion;
use Illuminate\Http\Request;

class ConversationQuestionService extends Service
{
    public function model()
    {
        return 'App\Models\ConversationQuestion';
    }


    public function create(Request $request)
    {

        $conversationQuestion = $request->except(['_token','_method']);

        $savedPage = $this->model->create($conversationQuestion);

        $savedPage->log()->create(['text'=>'Create ConversationQuestion #'.$savedPage->id.' '.$savedPage->title,'user_id'=>auth()->user()->id]);
        
        return true;
        
    }

    public function update(Request $request,ConversationQuestion $conversationQuestion)
    {
        $conversationQuestionData = $request->except(['_token','_method']);
        
        $conversationQuestion->update($conversationQuestionData);
        $conversationQuestion->log()->create(['text'=>'update ConversationQuestion #'.$conversationQuestion->id.' '.$conversationQuestion->title,'user_id'=>auth()->user()->id]);
        return true;
    }

    public function delete(ConversationQuestion $conversationQuestion)
    {
        $conversationQuestion->log()->create(['text'=>'Delete ConversationQuestion #'.$conversationQuestion->id.' '.$conversationQuestion->title,'user_id'=>auth()->user()->id]);
        return $conversationQuestion->delete();
    }
}