<?php

namespace App\Services;

use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Support\Facades\Hash;

class UserService extends Service
{
    
    public function model()
    {
        return 'App\Models\User';
    }
    public function userTypes()
    {
        return [0=>__('user.notSpecified'),1=>__('user.clientContact'),2=>__('user.purcheseRep'),3=>__('user.worker')];
    }


    public function create(UserStoreRequest $request)
    {
        $validatedUser = $request->validated();

        $user = $request->except(['_token','_method','type']);

        $user['password'] = Hash::make($user['password']);
        $savedUser = $this->model->create($user);
        if(isset($request->type))
        {
            foreach($request->type as $item)
            {
                $type = new UserType([
                    'user_id' => $savedUser->id,
                    'type_id' => 0
                ]);
                $savedUser->type()->save($type);
            }
        }

        $savedUser->log()->create(['text'=>'Create User #'.$savedUser->id.' '.$savedUser->name,'user_id'=>auth()->user()->id]);
        return $savedUser->assignRole(request()->role_id);
        
    }

    public function update(UserUpdateRequest $request,User $user)
    {
        $validatedUser = $request->validated();

        $userData = $request->except(['_token','_method','password']);
        if(isset($request->password))
        {
            $userData['password'] = Hash::make(request()->password);
        }
        $user->update($userData);
        if(isset($request->type))
        {
            $user->type()->delete();
            foreach($request->type as $item)
            {
                $type = new UserType([
                    'user_id' => $user->id,
                    'type_id' => 0
                ]);
                $user->type()->save($type);
            }
        }
        $user->log()->create(['text'=>'update User #'.$user->id.' '.$user->name,'user_id'=>auth()->user()->id]);
        return $user->syncRoles(request()->role_id);
    }

    public function delete(User $user)
    {
        $user->log()->create(['text'=>'Delete User #'.$user->id.' '.$user->name,'user_id'=>auth()->user()->id]);
        return $user->delete();
    }

}