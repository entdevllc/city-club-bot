<?php

namespace App\Services;

use App\Models\FbBot;
use Illuminate\Http\Request;

class FbBotService extends Service
{
    public function model()
    {
        return 'App\Models\FbBot';
    }


    public function create(Request $request)
    {

        $page = $request->except(['_token','_method']);

        $savedBot = $this->model->create($page);

        $savedBot->log()->create(['text'=>'Create FbBot #'.$savedBot->id.' '.$savedBot->title,'user_id'=>auth()->user()->id]);
        
        return true;
        
    }

    public function update(Request $request,FbBot $page)
    {
        $pageData = $request->except(['_token','_method']);
        
        $page->update($pageData);
        $page->log()->create(['text'=>'update Bot #'.$page->id.' '.$page->title,'user_id'=>auth()->user()->id]);
        return true;
    }

    public function delete(FbBot $page)
    {
        $page->log()->create(['text'=>'Delete Bot #'.$page->id.' '.$page->title,'user_id'=>auth()->user()->id]);
        return $page->delete();
    }
}