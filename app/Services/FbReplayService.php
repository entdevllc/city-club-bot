<?php

namespace App\Services;

use App\Models\FbBot;
use App\Models\FbReplay;
use App\Models\ReplayButton;
use Illuminate\Http\Request;

class FbReplayService extends Service
{
    public function model()
    {
        return 'App\Models\FbReplay';
    }


    public function create(Request $request)
    {

        $page = $request->except(['_token','_method','buttons','title','report_name']);

        $savedBot = $this->model->create($page);
        ReplayButton::where('id',$request->replay_button_id)->update(['report_name'=>$request->report_name]);
        if(is_array($request->buttons))
        {
            $button = [];
            foreach($request->buttons as $item)
            {
                if(isset($item['conversation']))
                {
                    $val = $item['conversation'];
                }elseif(isset($item['value']))
                {
                    $val = $item['value'];
                }else{
                    $val = '';
                }
                if(isset($item['title']))
                {
                    $button[] = new ReplayButton([
                        'fb_replay_id' => $savedBot->id,
                        'title' => $item['title'],
                        'val' => $val,
                        'postback_id' => md5($item['title'].microtime()),
                        'type' => 'postback'
                    ]);
                }
            }
            $savedBot->save();
            if(count($button)>0)
            {
                $savedBot->buttons()->saveMany($button);
            }
        }

        $savedBot->log()->create(['text'=>'Create FbReplay #'.$savedBot->id.' '.$savedBot->text,'user_id'=>auth()->user()->id]);
        
        return true;
        
    }

    public function updateMain(Request $request)
    {
        $replay = FbReplay::where('replay_button_id',0)->where('fb_bot_id',$request->fb_bot_id)->first();
        $replay->update(['text'=>$request->text]);

        if(is_array($request->buttons))
        {
            foreach($request->buttons as $item)
            {
                if($item['buttonType'] == 'conversation')
                {
                    $val = $item['conversation'];
                }elseif(isset($item['value']))
                {
                    $val = $item['value'];
                }else{
                    $val = '';
                }
                if(trim($item['title'])!='')
                {
                    $button = new ReplayButton([
                        'fb_replay_id' => $replay->id,
                        'title' => $item['title'],
                        'postback_id' => md5($item['title'].microtime()),
                        'type' => $item['buttonType'],
                        'val' => $val,
                        'report_name' => $request->title.' '.$item['title']
                    ]);
                    $button->save();
                }
            }
        }

        $replay->log()->create(['text'=>'update mainreplay bot #'.$request->fb_bot_id,'user_id'=>auth()->user()->id]);
        return true;
    }
    public function update(Request $request)
    {
        if($request->replay_button_id==0)
        {
            return $this->updateMain($request);
        }
        $replayButton = ReplayButton::with('childReplay')->where('id',$request->replay_button_id)->first();
        if(isset($request->title))
        {
            $replayButton->update(['title'=>$request->title,'report_name'=>$request->report_name]);
        }
        $replayButton->childReplay()->update(['text'=>$request->text]);

        if(is_array($request->buttons))
        {
            foreach($request->buttons as $item)
            {
                if(trim($item['title'])!='')
                {
                    $button = new ReplayButton([
                        'fb_replay_id' => $replayButton->childReplay->id,
                        'title' => $item['title'],
                        'postback_id' => md5($item['title'].microtime()),
                        'type' => $item['buttonType'],
                        'report_name' => $request->title.' '.$item['title']
                    ]);
                    $button->save();
                }
            }
        }

        $replayButton->log()->create(['text'=>'update Rplay #'.$replayButton->id.' '.$replayButton->title,'user_id'=>auth()->user()->id]);
        return true;
    }

    public function delete(ReplayButton $page)
    {
        $replayID = $page->replay->replay_button_id;
        $botID = $page->replay->fb_bot_id;
        $page->log()->create(['text'=>'Delete Replay button #'.$page->id.' '.$page->text,'user_id'=>auth()->user()->id]);
        $page->delete();
        return ['replayButton'=>$replayID,'fbBot' => $botID];
    }
}