<?php

namespace App\Services;

use Illuminate\Container\Container as App;

abstract class Service
{
    private $app;
    public $model;
    protected $newModel;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    public abstract function model();

    public function makeModel()
    {
        return $this->setModel($this->model());
    }

    public function setModel($eloquentModel)
    {
        $this->newModel = $this->app->make($eloquentModel);

        return $this->model = $this->newModel;
    }

}
