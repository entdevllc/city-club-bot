<?php

namespace App\Helpers;

use App\Models\Log;
use DateTimeInterface;


trait ModelHelper
{
    
    public function log()
    {
        return $this->morphMany( Log::class, 'logable');
    }
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}