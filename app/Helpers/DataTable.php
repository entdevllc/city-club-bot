<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

trait DataTable
{

    public function dataTable(Request $request)
    {

        $start = ($request->input('start')) ? $request->input('start') : 0;
        $length = ($request->input('length')) ? $request->input('length') : 50;
        $draw = ($request->input('draw')) ? $request->input('draw') : 1;
        $perPage = floor($start / $length) + 1;
        LengthAwarePaginator::currentPageResolver(function () use ($perPage) {
            return $perPage;
        });
        
        $data = $this->service->model;

        if(isset($request->order[0]['dir']))
        {
            $data = $data->orderBy($request->columns[$request->order[0]['column']]['data'], $request->order[0]['dir']);
        }

        foreach ($this->dataTableSearch($request) as $searchItem)
        {
            if($searchItem['exp']=='in')
            {
                $data = $data->whereIn($searchItem['field'], $searchItem['value']);
            }elseif($searchItem['exp']=='month')
            {
                $data = $data->whereMonth($searchItem['field'], $searchItem['value']);
            }elseif($searchItem['exp']=='year')
            {
                $data = $data->whereYear($searchItem['field'], $searchItem['value']);
            }else
            {
                $data = $data->where($searchItem['field'],$searchItem['exp'], $searchItem['value']);
            }
        }

        $query = $data->paginate($length, ['*']);
        $items = $query->items();


        foreach ($items as $key => $value) {
            $items[$key] = $this->dataTableColumns( $value );
        }

        $d['data'] = $items;
        $d['draw'] = $draw;
        $d['recordsTotal'] = $query->total();
        $d['recordsFiltered'] = $query->total();
        return Response()->json($d);
    }

    public function dataTableColumns($value)
    {
        return $value;
    }

    public function dataTableSearch(Request $request)
    {
        $search = [];
        if(isset($request->search['value']))
        {
            $search = [['field'=>'name','exp'=>'LIKE','value'=>'%'.$request->search['value'].'%']];
        }
        return $search;
    }
}