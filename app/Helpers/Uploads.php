<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

trait Uploads
{
    
    public function uploadImage($file, $type, $folder) {
        try {
            $ext = strtolower($file->getClientOriginalExtension());

            if (!in_array($ext, explode(',',getSetting('uploadFileExt'.$type)))) {
                return false;
            }

            $subfolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
            $destinationPath = storage_path('app/public/uploads/' . $folder . '/' . $subfolder);
            $fileName = md5($file->getClientOriginalName()) . '-' . rand(9999, 9999999) .
                    '-' . rand(9999, 9999999) . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $fileName);

            return '/'.$folder . '/' . $subfolder . $fileName;
            
        } catch (FileException $exception) {
            dd($exception);
            return false;
        }
    }
}