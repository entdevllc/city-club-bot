<?php

use App\Models\Setting;

function getSetting($var){
		return isset(Setting::where('var',$var)->first()->val)?Setting::where('var',$var)->first()->val:'';
}

function verifyPermission($permissions){
	$permissionsCount = count($permissions);
	if($permissionsCount==0)
	{
		return true;
	}
	foreach($permissions as $permission)
	{
		if(auth()->user()->can($permission))
		{
			return true;
		}
	}
	return false;
}
function days_in_month($month, $year){
    return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
} 