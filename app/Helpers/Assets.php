<?php

namespace App\Helpers;


class Assets
{
    private static $scriptAssets = [];
    private static $styleAssets = [];

    public static function getScriptAssets()
    {
        return self::$scriptAssets;
    }

    public static function setScriptAssets($name, $path)
    {
        if(!self::isScriptLoaded($name))
        {
            self::$scriptAssets[$name] = $path;
        }
    }
    
    public static function isScriptLoaded($name)
    {
        
        if(array_key_exists($name,self::$scriptAssets))
        {
            return true;
        }else{
            return false;
        }
    }

    public static function getStyleAssets()
    {
        return self::$styleAssets;
    }

    public static function setStyleAssets($name, $path)
    {
        if(!self::isStyleLoaded($name))
        {
            self::$styleAssets[$name] = $path;
        }
    }
    
    public static function isStyleLoaded($name)
    {
        
        if(array_key_exists($name,self::$styleAssets))
        {
            return true;
        }else{
            return false;
        }
    }
}