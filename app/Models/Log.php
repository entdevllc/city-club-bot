<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory, ModelHelper;
    
    protected $guarded = [];

    public function logable()
    {
        return $this->morphTo();
    }
    public function user()
    {
        return $this->belongsTo(User::class);

    }
}
