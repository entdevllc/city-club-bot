<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FbBot extends Model
{
    use HasFactory, ModelHelper;
    protected $guarded = [];

    function fbPage()
    {
        return $this->belongsTo(FbPage::class,'page_id');
    }
}
