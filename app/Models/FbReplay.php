<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FbReplay extends Model
{
    use HasFactory, ModelHelper, SoftDeletes;
    protected $guarded = [];

    public function buttons()
    {
        return $this->hasMany(ReplayButton::class);
    }
}
