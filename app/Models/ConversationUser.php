<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConversationUser extends Model
{
    use HasFactory, ModelHelper;
    protected $guarded = [];
    function conversation()
    {
        return $this->belongsTo(Conversation::class,'conversation_id');
    }
    function conversationAnswer()
    {
        return $this->hasMany(ConversationAnswer::class,'conversation_user_id');
    }
}
