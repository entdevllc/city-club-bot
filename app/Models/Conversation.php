<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use HasFactory, ModelHelper;
    protected $guarded = [];
    function fbBot()
    {
        return $this->belongsTo(FbBot::class,'fb_bot_id');
    }

    function conversationQuestion()
    {
        return $this->hasMany(ConversationQuestion::class,'conversation_id');
    }
}
