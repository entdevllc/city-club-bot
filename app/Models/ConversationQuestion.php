<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConversationQuestion extends Model
{
    use HasFactory, ModelHelper;
    protected $guarded = [];
    function conversation()
    {
        return $this->belongsTo(Conversation::class,'conversation_id');
    }
}
