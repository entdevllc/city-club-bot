<?php

namespace App\Models;

use App\Helpers\ModelHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReplayButton extends Model
{
    use HasFactory, ModelHelper;
    protected $guarded = [];
    public function replay()
    {
        return $this->belongsTo(FbReplay::class,'fb_replay_id');
    }

    public function childReplay()
    {
        return $this->belongsTo(FbReplay::class,'id','replay_button_id');
    }
}
