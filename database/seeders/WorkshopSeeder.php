<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkshopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'Add Workshop Workers',
                'guard_name' => 'web',
            ],
            [
                'name' => 'View Workshop Workers',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Edit Workshop Workers',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Delete Workshop Workers',
                'guard_name' => 'web',
            ],[
                'name' => 'Add Workshop Sections',
                'guard_name' => 'web',
            ],
            [
                'name' => 'View Workshop Sections',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Edit Workshop Sections',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Delete Workshop Sections',
                'guard_name' => 'web',
            ],[
                'name' => 'Add Suppliers',
                'guard_name' => 'web',
            ],
            [
                'name' => 'View Suppliers',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Edit Suppliers',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Delete Suppliers',
                'guard_name' => 'web',
            ],[
                'name' => 'Add Purchase',
                'guard_name' => 'web',
            ],
            [
                'name' => 'View Purchase',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Edit Purchase',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Delete Purchase',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Approve Purchase',
                'guard_name' => 'web',
            ]
        ]);
    }
}
