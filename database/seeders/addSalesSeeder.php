<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class addSalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'Add Sales',
                'guard_name' => 'web',
            ],
            [
                'name' => 'View Sales',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Edit Sales',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Delete Sales',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Representative',
                'guard_name' => 'web',
            ]
            ]
        );
    }
}
