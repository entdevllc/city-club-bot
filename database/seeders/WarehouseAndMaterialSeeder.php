<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WarehouseAndMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([[
            'name' => 'Add Materials',
            'guard_name' => 'web',
        ],
        [
            'name' => 'View Materials',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Edit Materials',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Delete Materials',
            'guard_name' => 'web',
        ],[
            'name' => 'Add Warehouses',
            'guard_name' => 'web',
        ],
        [
            'name' => 'View Warehouses',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Edit Warehouses',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Delete Warehouses',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Add Materials Quantity',
            'guard_name' => 'web',
        ]]);
    }
}
