<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCurrentActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_current_actions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('current_action');
            $table->integer('action_id')->nullable();
            $table->integer('step_id')->nullable();
            $table->timestamp('stop_until')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_current_actions');
    }
}
