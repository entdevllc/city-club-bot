<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFbBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_bots', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });

        DB::table('permissions')->insert([[
            'name' => 'Add fbBot',
            'guard_name' => 'web',
        ],
        [
            'name' => 'View fbBot',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Edit fbBot',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Delete fbBot',
            'guard_name' => 'web',
        ]
    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_bots');
    }
}
