<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUuidFieldToConversationAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conversation_answers', function (Blueprint $table) {
            $table->string('conversation_user_id');
            $table->integer('conversation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conversation_answers', function (Blueprint $table) {
            $table->dropColumn('conversation_user_id','conversation_id');
        });
    }
}
