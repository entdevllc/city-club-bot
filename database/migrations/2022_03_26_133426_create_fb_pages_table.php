<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFbPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_pages', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('token')->nullable();
            $table->timestamps();
        });

        DB::table('permissions')->insert([[
            'name' => 'Add fbPage',
            'guard_name' => 'web',
        ],
        [
            'name' => 'View fbPage',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Edit fbPage',
            'guard_name' => 'web',
        ],
        [
            'name' => 'Delete fbPage',
            'guard_name' => 'web',
        ]
    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_pages');
    }
}
